class SDatum(object):
    """
    Pseudo Datum object to facilitate the notion of first block in data.
    """
    def __init__(self):
        self.tup = '^*_^*'
        self.word = self.tag = '^*'
        self.n = None
        self.b = self

    def __str__(self):
        return self.tup


class EDatum(object):
    """
    Pseudo Datum object to facilitate the notion of last block in data.
    """
    def __init__(self):
        self.tup = '^$_^$'
        self.word = self.tag = '^$'
        self.n = self
        self.b = None

    def __str__(self):
        return self.tup


class Datum(object):
    """
    Single data record for book keeping and better features examination
    """
    default_n = EDatum()
    default_b = SDatum()

    def __init__(self, tup, n=default_n, b=default_b):
        self.tup = tup
        self.word, self.tag = self.tup.split('_')
        self.word = self.word.lower()
        self.n = n
        self.b = b
        if isinstance(self.b, Datum):
            self.b.n = self
        self.index = None
        self.capital = False

    def __str__(self):
        return self.tup

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        return self.__str__()
