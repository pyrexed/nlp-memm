import csv
import itertools
import logging
import os
import sys
from collections import OrderedDict, defaultdict

from datum import Datum, SDatum
from features import CFeatures, BFeatures
from mclc import MCLC

logger = logging.getLogger('memm')
logger.setLevel(logging.DEBUG)
ch = logging.FileHandler("memm.log", mode='a')
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)


def main(train_file, test_file, complex_=True):
    """
    Main program flow
    :param train_file: path to train file
    :param test_file: path to test file
    :param complex_: Bool, if true uses the complex language model, if false uses the basic language model
    :return: list<string>, the test/comp predicted tags
    """
    logger.debug("Starting Memm")
    data, word_to_tag = parse_train_wtag(train_file)
    if complex_:
        fa = CFeatures(data, word_to_tag)
    else:
        fa = BFeatures(data, word_to_tag)

    memm = MCLC(fa, data, word_to_tag)
    memm.train()

    # loading test/comp data
    if check_file(test_file):
        test_words, testtags = parse_test_wtag(test_file)

        # inference
        pred = memm.inference(test_words)

        # Confusion matrix and accuracy calculation
        conf, index_map = memm.confusion_matrix(pred, testtags)
        acc = memm.accuracy(conf)
        with open(os.path.join(path, "output.csv"), "wb") as f:
            writer = csv.writer(f)
            writer.writerow(["Test Words"] + test_words)
            writer.writerow(["Predictions"] + pred)
            writer.writerow(["Real Test Tags"] + testtags)
            writer.writerow([])
            writer.writerow([""] + index_map.values())
            for ind, key in index_map.items():
                writer.writerow([key] + list(conf[ind]))
            writer.writerow([])
            writer.writerow(["Accuracy", acc])
    else:
        test_words = parse_comp_wtag(test_file)
        pred = memm.inference(test_words)
        write_output(test_file, pred, complex_)
    return pred


def check_file(filepath):
    """
    checks if file is a test file (has tags) or comp file (only words)
    :param filepath: the path to the file
    :return: True, if file has tags, false otherwise
    """
    with open(filepath, "rb") as f:
        return '_' in f.next().split()[0]


def parse_train_wtag(filepath):
    """
    Parses the training data
    :param filepath: String, path to wtag file
    :return: Data, Prob, both are dictionaries, the first stores integer indexes as keys and datums as values, the scond
    stores word_tag string as key, and count of their appearances.
    """
    logger.debug("Start loading training data")
    data = OrderedDict()
    word_to_tag = defaultdict(set)
    back = SDatum()
    count = itertools.count()
    with open(filepath, "r") as f:
        for line in f:
            for tup in line.split():
                tempd = Datum(tup, b=back)
                tempd.index = count.next()
                back = tempd
                tempd.capital = tup[0].isupper()
                # map index to tuple (word_tag)
                data[tempd.index] = tempd

                # map word to possible (seen) tags.
                word_to_tag[tempd.word].add(tempd.tag)
    logger.debug("Finished loading training data")
    return data, word_to_tag


def write_output(test_file, pred, complex_):
    """
    writes output predictions to file
    :param test_file: path to test file
    :param pred: list of predictions
    :param complex_: whter using language model 1 or 2
    :return:
    """
    c = 0
    if not complex_:
        fn = "comp_m1_200467918.wtag"
    else:
        fn = "comp_m2_200467918.wtag"
    fp = os.path.join(path, fn)
    to_write = []
    with open(test_file, "rb") as input_:
        for line in input_:
            line_words = line.split()
            c_w = len(line_words)
            line_pred = pred[c:c + c_w]
            line_output = ' '.join(map('_'.join, zip(line_words, line_pred)))
            c += c_w
            to_write.append(line_output)
    with open(fp, "wb") as output:
        output.write('\r\n'.join(to_write))


def parse_test_wtag(filepath):
    """
    Parses the test data
    :param filepath: String, path to wtag file
    :return: List<string>,List<string>, list of words and list of tags
    """
    logger.debug("Started loading testing data")
    word_list = []
    tag_list = []
    with open(filepath, "r") as f:
        for line in f:
            for tup in line.split():
                word, tag = tup.split('_')
                word_list.append(word)
                tag_list.append(tag)
    logger.debug("Finished loading testing data")
    return word_list, tag_list


def parse_comp_wtag(filepath):
    """
    Parsing comp.wtag
    :param filepath: str, path fo comp.wtag file
    :return: List<str>, list of words parsed from file
    """
    logger.debug("Started loading comp data")
    word_list = []
    with open(filepath, "r") as f:
        for line in f:
            for word in line.split():
                word_list.append(word)
    logger.debug("Finished loading comp data")
    return word_list


if __name__ == "__main__":
    filename = 'profile.stats'
    path = os.path.dirname(__file__)
    tr_file = os.path.join(path, "train.wtag")
    te_file = os.path.join(path, "comp.words")
    comp = True
    if len(sys.argv) == 4:
        tr_file = sys.argv[1]
        te_file = sys.argv[2]
        if sys.argv[3].lower() == "complex":
            comp = True
        elif sys.argv[3].lower() == "basic":
            comp = False
        else:
            print "Usage: python main.py <train.wtag path> <test.wtag|comp.words path> <complex|basic>"
    elif len(sys.argv) > 1:
        print "Usage: python main.py <train.wtag path> <test.wtag|comp.words path> <complex|basic>"

    print main(tr_file, te_file, comp)
