import cPickle as pickle
import logging
import os

import numpy as np
from scipy.optimize import fmin_l_bfgs_b as minimize

logger = logging.getLogger("memm")


class MCLC(object):
    """
    Multi Class Logistic Regression (sort of...)
    """

    def __init__(self, fobj, data, word_to_tag, reg=-10.0):
        """
        Constructor for multi-class linear regression (effective MEMM).
        :param fobj, CFeatures, instantiated features object
        :param data: Dict<int, str>, map index to Datum object (containing the tuple and all related data.
        :param word_to_tag: Dict<str, list<str>>, maps a word (str) to all seen tags during training.
        :param reg: int, optional, regularizer value (effective lambda need to be used with (-) minus sign).
        :return:
        """
        self.reg = reg  # Effective lambda value
        self.fobj = fobj
        # if not self.load_features():
        self.features, self.fbx, self.mapping = self.fobj.extract_features()
        self.tag_index = self.fobj.tag_index
        self.index_tag = self.fobj.index_tag
        logger.debug("Finished extracting features")
        self.v = np.zeros(self.features.shape[1], dtype=np.float64)
        self.word_to_tag = word_to_tag  # maps word to a set of possible (seen) tags
        self.data = data  # maps an index in sparse matrix to datum object (word, tag)
        self.x_map = set(self.word_to_tag.keys())  # set of all seen words
        # dictionary of key: word(x); value: sparse matrix with shape (len(x), len(feature_stack))
        self.fsv = np.squeeze(np.asarray(self.features.sum(axis=0), dtype=np.float64))
        self.mappingkeys = self.mapping.keys()
        self.tag_index = self.fobj.tag_index
        self.index_tag = self.fobj.index_tag

    def smax(self, v):
        """
        Calculates the log probability of all words seen.
        :param v: np.array, Weights vector
        :return: log(P(y|x)) of all words
        """
        ret = []
        for i in self.mappingkeys:
            xfv = self.features[i]  # x feature vector sparse feature vector for word
            xsm = self.fbx[i]  # x sub matrix (sparse matrix per word)
            ret.append(np.asscalar(xfv.dot(v)) - np.log(np.sum(np.exp(xsm.dot(v)))))
        return np.array(ret)

    @staticmethod
    def confusion_matrix(y_pred, y_test):
        """
        Creates a confusion matrix
        :param y_pred: List<String> ordered by appearance predicted tags
        :param y_test: List<string> Ordered by appearance test tags
        :return: NP.matrix, Dict<int,String> - the confusion matrix and the mapping from index to tag
        """
        all_y = set(y_pred).union(y_test)
        index_map = dict(map(lambda (x, y): (y, x), enumerate(all_y)))
        nc = len(all_y)
        m = np.zeros((nc, nc), dtype=np.uint32)
        for i, j in zip(y_pred, y_test):
            m[index_map[i], index_map[j]] += 1
        return m, index_map.__class__(map(reversed, index_map.items()))

    @staticmethod
    def accuracy(conf):
        """
        Calculates the accuracy of prediction from a confusion matrix
        :param conf:np.Matrix, symetric confusion matrix
        :return: Float, The accuracy
        """
        return float(np.trace(conf)) / float(np.sum(conf))

    def mlf(self, v):
        """
        modified loss function
        Derives smax over all words.
        :param v:ndarray/matrix of shape MX1
        :return:
        """
        logger.debug("mlf calculation started, top v values: " + str(sorted(v)[:5]))
        regularizer = np.dot(v, v) * self.reg * 0.5
        value = -np.add(np.sum(self.smax(v)), regularizer)
        logger.debug("mlf calculation ended, value: " + str(value))
        return value

    def grad(self, v):
        """
        Given a current v vector if weights, calculates the gradient of mlf
        :param v: np.array/matrix of shape (1,len(features)), feature weights
        :return: np.array size of len(features) of the current gradient.
        """
        expected = np.zeros(self.features.shape[1], dtype=np.float64)
        for i in self.mappingkeys:
            xsm = self.fbx[i].toarray()  # x sub matrix (sparse matrix per word)
            exv = np.exp(xsm.dot(v))
            yp = exv / np.sum(exv)
            expected += (xsm.T * yp).sum(axis=1)  # effective f(x(i),y')p(y'|x(i);v)
        grad = np.asarray(expected - self.fsv - v * self.reg)
        return grad

    def train(self, load_from_file=False):
        """
        Main method that controls the training flow, and most importantly runs the lbfgs algorithm.
        :param load_from_file: if True, v will be loaded from file and the whole training will be skipped.
        :return:
        """
        if load_from_file:
            if self.load_v():
                logger.debug("Loaded v from file")
        else:
            logger.debug("Starting Training")
            v = np.zeros(self.features.shape[1], dtype=np.float64)
            res = minimize(self.mlf, v, fprime=self.grad, factr=1e12, pgtol=1e-3)  # approx_grad=True,
            self.v = res[0]
            logger.debug("Finished training")
            logger.debug("Result: success: %s; message: %s;" % (res[2]['warnflag'], res[2]['task']))
            if res[2]['warnflag'] == 0:
                # if the training converged, the v and the features are saved to file
                self.save_v()
                self.save_features()
            else:
                raise Exception("Did not converge.")
        logger.debug("Top features: " + str(sorted(self.v)[:5]))

    def inference(self, words):
        """
        Deducts the pos tags fir the list of words given, uses viterbi algorithm
        :param words: list of words to inference on.
        :return: tags: list of strings representing the tags (the list is ordered the same way the words are)
        """
        logger.debug("Starting inference.")
        # sets the CFeatures object to have a tag->index mapping (dictionary)

        # instantiation of pi, back-pointers
        shape = (len(self.tag_index), len(words))
        pi = np.zeros(shape, dtype=np.float64)
        bp = -np.ones(shape, dtype=np.int8)

        # initialization of first column of the pi matrix with the first observation
        # checks to see if the word is in the seen words
        # tag_fv is a Dictionary tag_index-> feature_vector
        tag_fv = self.fobj.extract_single(words[0], [('^*', '^*')], words[0].lower() in self.x_map)

        # the divider in the probability (equal to log(Sum[y`Ey](e(f*v)))
        basep = np.log(np.sum(np.exp(np.array(tag_fv.values()).dot(self.v))))

        # calculating each vector dividee (equal to e(f*v)) and total probability log(p(y|x;v))
        pi[:, 0].fill(-basep)
        for ind, vec in tag_fv.items():
            pi[ind, 0] = np.asscalar(self.v.dot(vec) - basep)
        bp_ind = np.argmax(pi[:, 0], axis=0)  # a flatten indices of the backpointer
        llt_lt = [('^*', self.index_tag[bp_ind])]
        # populating the pi and bp matrices accordingly (same as above but over the rest of observations)
        first_iteration = True
        for i in xrange(1, len(words)):
            trans = np.zeros((1, len(self.tag_index)))
            trans.fill(-np.inf)
            tag_fv = self.fobj.extract_single(words[i], llt_lt, words[i].lower() in self.x_map)
            basep = np.log(np.sum(np.exp(np.array(tag_fv.values()).dot(self.v))))

            # trans is the q vector equivalent to the one in the viterbi algorithm in the lectures
            for ind, vec in tag_fv.items():
                trans[0, ind] = np.asscalar(self.v.dot(vec) - basep)

            # pi*q in viterbi algorithm
            i_mat = pi[:, i - 1, None] + trans

            # finding argmax in the pi*q and assigning backpointer to each index in the column
            bp[:, i] = np.argmax(i_mat, axis=0)  # The correct axis is: 0
            lt_ind = bp[:, i]
            llt_ind = bp[lt_ind, i - 1]
            lt_tag = map(self.index_tag.get, lt_ind)
            if not first_iteration:
                llt_tag = map(self.index_tag.get, llt_ind)
            else:
                llt_tag = ["^*"] * len(llt_ind)
                first_iteration = False
            llt_lt = zip(llt_tag, lt_tag)
            # finding max probability in the pi*q and assigning the probability to each index in the column
            pi[:, i] = np.amax(i_mat, axis=0)  # The correct axis is: 0
            assert np.max(pi[:, i]) > -np.inf

        # finished inference, starting the backtracking
        # finding the last chosen probability
        tags_index = [pi[:, -1].argmax()]
        for j in xrange(len(words) - 1, 0, -1):
            # iterating over the backpointers, choosing the maximal each time
            tags_index.append(bp[tags_index[-1], j])

        # translating the indexes chosen back to tags (as string)
        tags = []
        for i in tags_index:
            tags.insert(0, self.index_tag[i])
        logger.debug("Finished inference.")
        return tags

    def save_v(self):
        """
        Saves the v to a binary file (serialization
        :return:
        """
        with open('v.pkl', 'wb') as output:
            pickle.dump(self.v, output, pickle.HIGHEST_PROTOCOL)

    def save_features(self):
        """
        Saves some of the features data structures to file
        :return:
        """
        with open('features.pkl', 'wb') as output:
            pickle.dump((self.features, self.fbx, self.mapping, self.tag_index, self.index_tag), output,
                        pickle.HIGHEST_PROTOCOL)

    def load_features(self):
        """
        Loads the feature from file into the object data members
        :return: True if file exists, false otherwise
        """
        if os.path.exists('features.pkl'):
            with open('features.pkl', 'rb') as input_:
                self.features, self.fbx, self.mapping, self.tag_index, self.index_tag = pickle.load(input_)
                return True
        return False

    def load_v(self):
        """
        Loads vector v of weight from binary file into data members: self.v
        :return: True if file exists, false otherwise
        """
        if os.path.exists('v.pkl'):
            with open('v.pkl', 'rb') as input_:
                self.v = pickle.load(input_)
                return True
        return False
