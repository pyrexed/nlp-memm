import logging
from collections import OrderedDict, Counter
from itertools import count

import numpy as np
from scipy.sparse import csr_matrix

logger = logging.getLogger("memm")


class CFeatures(object):
    def __init__(self, data, word_to_tag):
        """
        Feature generation and extraction for complex model
        :param data: Dict<int,Datum>, maps index to Datum object
        :return:
        """
        self.feature_stack = OrderedDict()
        self.data = data
        self.word_to_tag = word_to_tag
        self.d_ind_features = OrderedDict()
        self.d_alt_features = OrderedDict()
        self.generate_features()
        x = []
        y = []
        for v in data.itervalues():
            x.append(v.word)
            y.append(v.tag)
        assert len(x) == len(y)
        self.tag_index = dict([(j, i) for i, j in enumerate(set(y))])  # map tag->index in prob matrix
        self.index_tag = dict(enumerate(set(y)))  # map index->tag in prob matrix

    def generate_features(self):
        """
        Generates the various features to be extracted from training data
        :return:
        """
        logger.debug("Starting feature generation.")
        c = count()
        feature_list = []
        feature_counts = [0] * 9
        for d in self.data.itervalues():
            features = [d.word + '_' + d.tag + "_100"]  # word tag feature
            features.extend([d.word[i:] + '_' + d.tag + "_101" for i in range(-4, 0)])  # suffixes feature
            features.extend([d.word[:i] + '_' + d.tag + "_102" for i in range(1, 5)])  # prefixes feature
            features.append(d.b.b.tag + '_' + d.b.tag + '_' + d.tag + "_103")  # trigram feature
            features.append(d.b.tag + '_' + d.tag + "_104")  # bigram feature
            features.append(d.tag + "_105")  # unigram feature

            # from here on are features created by us, they will be noted with the suffix: _1,_2,...
            if d.capital:
                features.append("cap_" + d.tag + "_1")  # Should help NNP
                if '.' in d.word:
                    features.append("._" + d.tag + "_2")  # Should help with words like Mr. Corp.
            if any(char.isdigit() for char in d.word):
                features.append("num_" + d.tag + "_3")  # should help with CC
            features = set(features)
            feature_list.extend(features)
            self.d_ind_features[d.index] = features

            # We create alternative feature for other y'Ey for only other tags seen with this word
            for alt_tag in self.word_to_tag[d.word]:
                alt_features = [d.word + '_' + alt_tag + "_100"]
                alt_features.extend([d.word[i:] + '_' + alt_tag + "_101" for i in range(-4, 0)])
                alt_features.extend([d.word[:i] + '_' + alt_tag + "_102" for i in range(1, 5)])
                alt_features.append(d.b.b.tag + '_' + d.b.tag + '_' + alt_tag + "_103")
                alt_features.append(d.b.tag + '_' + alt_tag + "_104")
                alt_features.append(alt_tag + "_105")
                if d.capital:
                    alt_features.append("cap_" + d.tag + "_1")  # Should help NNP
                    if '.' in d.word:
                        alt_features.append("._" + d.tag + "_2")  # Should help with words like Mr. Corp.
                if any(char.isdigit() for char in d.word):
                    alt_features.append("num_" + d.tag + "_3")  # should help with CC
                alt_features = set(alt_features)
                feature_list.extend(alt_features)
                if d.index in self.d_alt_features:
                    self.d_alt_features[d.index].append(alt_features)
                else:
                    self.d_alt_features[d.index] = [alt_features]
        bin_count_features = Counter(feature_list)
        for feature, count_ in bin_count_features.iteritems():
            if feature.endswith("_100") and count_ > 3:
                self.feature_stack[feature] = c.next()
                feature_counts[0] += 1
            elif feature.endswith("_101") and count_ > 10:
                self.feature_stack[feature] = c.next()
                feature_counts[1] += 1
            elif feature.endswith("_102") and count_ > 10:
                self.feature_stack[feature] = c.next()
                feature_counts[2] += 1
            elif feature.endswith("_103") and count_ > 3:
                self.feature_stack[feature] = c.next()
                feature_counts[3] += 1
            elif feature.endswith("_104") and count_ > 5:
                self.feature_stack[feature] = c.next()
                feature_counts[4] += 1
            elif feature.endswith("_105") and count_ > 0:
                self.feature_stack[feature] = c.next()
                feature_counts[5] += 1
            elif feature.endswith("_1") and count_ > 0:
                self.feature_stack[feature] = c.next()
                feature_counts[6] += 1
            elif feature.endswith("_2") and count_ > 0:
                self.feature_stack[feature] = c.next()
                feature_counts[7] += 1
            elif feature.endswith("_3") and count_ > 0:
                self.feature_stack[feature] = c.next()
                feature_counts[8] += 1
            elif count_ > 5:
                self.feature_stack[feature] = c.next()
        logger.debug("Finished generating features, total features generated: %d" % len(self.feature_stack))
        logger.debug("Number of 100: %d" % feature_counts[0])
        logger.debug("Number of 101: %d" % feature_counts[1])
        logger.debug("Number of 102: %d" % feature_counts[2])
        logger.debug("Number of 103: %d" % feature_counts[3])
        logger.debug("Number of 104: %d" % feature_counts[4])
        logger.debug("Number of 105: %d" % feature_counts[5])
        logger.debug("Number of 1: %d" % feature_counts[6])
        logger.debug("Number of 2: %d" % feature_counts[7])
        logger.debug("Number of 3: %d" % feature_counts[8])

    def extract_single(self, word, llt_lt, seen):
        """
        Creates a feature vectors for each d, according to all possible (seen) tags for that d.word
        :param llt_lt: list of tuples, last tag and last last tag, each entry corresponds to the index
        :param word: String the word seen
        :param seen: Bool, whether the word was seen or not
        :return: Dict<tag, np.array>, tag as string, np.array is of zeros and ones, ones are where features are on.
        """
        alt_features = {}
        capital = word[0].isupper()
        word = word.lower()
        if seen:
            # if the word has not been seen before, we'll use every tag possible
            to_iterate = self.word_to_tag[word]
        else:
            # else we'll use only tags seen with that word
            to_iterate = self.tag_index.keys()

        for alt_tag in to_iterate:
            features = [word + '_' + alt_tag + "_100", alt_tag + "_105"]
            features.extend([word[i:] + '_' + alt_tag + "_101" for i in range(-4, 0)])
            features.extend([word[:i] + '_' + alt_tag + "_102" for i in range(1, 5)])
            for (llt, lt) in llt_lt:
                features.append(llt + '_' + lt + '_' + alt_tag + "_103")
                features.append(lt + '_' + alt_tag + "_104")
            if capital:
                features.append("cap_" + alt_tag + "_1")  # Should help NNP
                if '.' in word:
                    features.append("._" + alt_tag + "_2")  # Should help with words like Mr. Corp.
            if any(char.isdigit() for char in word):
                features.append("num_" + alt_tag + "_3")  # should help with CC
            features_ind = filter(None, map(self.feature_stack.get, features))
            fv = np.zeros(len(self.feature_stack))
            fv[features_ind] = 1
            alt_features[self.tag_index[alt_tag]] = fv
        return alt_features

    def extract_features(self):
        """
        Extracts features from the data, after deciding which features to save.
        :return:
        """
        logger.debug("Starting feature extraction.")
        cols = len(self.feature_stack)
        shape = (0, cols)

        # create a sparse feature matrix, assigned to each word seen, makes it easier later on to sum,
        # the vectors corresponds to the seen tags
        fbx = []

        # full scale, sparse matrix for all feature vector (vertical)
        sa = csr_matrix(shape, dtype=np.int32)
        for ind, features in self.d_ind_features.iteritems():
            indices = filter(None, map(self.feature_stack.get, features))
            arr = np.zeros(cols, dtype=np.int32)
            arr[indices] = 1
            csr_vappend(sa, csr_matrix(arr))
            alt_sparse = []
            for alt_features in self.d_alt_features[ind]:
                alt_indices = filter(None, map(self.feature_stack.get, alt_features))
                arr = np.zeros(cols, dtype=np.int32)
                arr[alt_indices] = 1
                alt_sparse.append(arr)
            fbx.append(csr_matrix(alt_sparse))
        logger.debug("Finished feature Extraction")
        return sa, fbx, self.d_ind_features


class BFeatures(object):
    def __init__(self, data, word_to_tag):
        """
        Feature generation and extraction for basic model
        :param data: Dict<int,Datum>, maps index to Datum object
        :return:
        """
        self.feature_stack = OrderedDict()
        self.data = data
        self.word_to_tag = word_to_tag
        self.d_ind_features = OrderedDict()
        self.d_alt_features = OrderedDict()
        self.generate_features()
        x = []
        y = []
        for v in data.itervalues():
            x.append(v.word)
            y.append(v.tag)
        assert len(x) == len(y)
        self.tag_index = dict([(j, i) for i, j in enumerate(set(y))])  # map tag->index in prob matrix
        self.index_tag = dict(enumerate(set(y)))  # map index->tag in prob matrix

    def generate_features(self):
        """
        Generates the various features to be extracted from training data
        :return:
        """
        logger.debug("Starting basic model feature generation.")
        c = count()
        feature_list = []
        feature_counts = [0, 0, 0]
        for d in self.data.itervalues():
            features = [d.word + '_' + d.tag + "_100", d.b.b.tag + '_' + d.b.tag + '_' + d.tag + "_103",
                        d.b.tag + '_' + d.tag + "_104"]
            features = set(features)
            feature_list.extend(features)
            self.d_ind_features[d.index] = features
            for alt_tag in self.word_to_tag[d.word]:
                alt_features = [d.word + '_' + alt_tag + "_100", d.b.b.tag + '_' + d.b.tag + '_' + alt_tag + "_103",
                                d.b.tag + '_' + alt_tag + "_104"]
                alt_features = set(alt_features)
                feature_list.extend(alt_features)
                if d.index in self.d_alt_features:
                    self.d_alt_features[d.index].append(alt_features)
                else:
                    self.d_alt_features[d.index] = [alt_features]
        for feature in set(feature_list):
            if feature.endswith("_100"):
                feature_counts[0] += 1
            elif feature.endswith("_103"):
                feature_counts[1] += 1
            elif feature.endswith("_104"):
                feature_counts[2] += 1
            self.feature_stack[feature] = c.next()
        logger.debug("Finished generating basic model features, total features generated: %d" % len(self.feature_stack))
        logger.debug("Number of 100: %d" % feature_counts[0])
        logger.debug("Number of 103: %d" % feature_counts[1])
        logger.debug("Number of 104: %d" % feature_counts[2])

    def extract_single(self, word, llt_lt, seen):
        """
        Creates a feature vectors for each d, according to all possible (seen) tags for that d.word
        :param llt_lt: list of tuples, last tag and last last tag, each entry corresponds to the index
        :param word: String the word seen
        :param seen: Bool, whether the word was seen or not
        :return: Dict<tag, np.array>, tag as string, np.array is of zeros and ones, ones are where features are on.
        """
        alt_features = {}
        word = word.lower()
        if seen:
            # if the word has not been seen before, we'll use every tag possible
            to_iterate = self.word_to_tag[word]
        else:
            # else we'll use only tags seen with that word
            to_iterate = self.tag_index.keys()

        for alt_tag in to_iterate:
            features = [word + '_' + alt_tag + "_100"]
            for (llt, lt) in llt_lt:
                features.append(llt + '_' + lt + '_' + alt_tag + "_103")
                features.append(lt + '_' + alt_tag + "_104")
            features_ind = filter(None, map(self.feature_stack.get, features))
            fv = np.zeros(len(self.feature_stack))
            fv[features_ind] = 1
            alt_features[self.tag_index[alt_tag]] = fv
        return alt_features

    def extract_features(self):
        """
        Extracts features from the data, after deciding which features to save.
        :return:
        """
        logger.debug("Starting basic model feature extraction.")
        cols = len(self.feature_stack)
        shape = (0, cols)

        # create a sparse feature matrix, assigned to each word seen, makes it easier later on to sum,
        # the vectors corresponds to the seen tags
        fbx = []

        # full scale, sparse matrix for all feature vector (vertical)
        sa = csr_matrix(shape, dtype=np.int32)
        for ind, features in self.d_ind_features.iteritems():
            indices = filter(None, map(self.feature_stack.get, features))
            arr = np.zeros(cols, dtype=np.int32)
            arr[indices] = 1
            csr_vappend(sa, csr_matrix(arr))
            alt_sparse = []
            for alt_features in self.d_alt_features[ind]:
                alt_indices = filter(None, map(self.feature_stack.get, alt_features))
                arr = np.zeros(cols, dtype=np.int32)
                arr[alt_indices] = 1
                alt_sparse.append(arr)
            fbx.append(csr_matrix(alt_sparse))
        logger.debug("Finished basic model feature Extraction")
        return sa, fbx, self.d_ind_features


def csr_vappend(a, b):
    """ Takes in 2 csr_matrices and appends the second one to the bottom of the first one.
    Much faster than scipy.sparse.vstack but assumes the type to be csr and overwrites
    the first matrix instead of copying it. The data, indices, and indptr still get copied.
    :param a: csr_matrix
    :param b: csr_matrix
    """
    a.data = np.hstack((a.data, b.data))
    a.indices = np.hstack((a.indices, b.indices))
    a.indptr = np.hstack((a.indptr, (b.indptr + a.nnz)[1:]))
    a._shape = (a.shape[0] + b.shape[0], b.shape[1])
